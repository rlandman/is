# is

Support for the following is checks:

is.empty: Checks if a value is empty.
Empty means:

- an empty string
- undefined
- null
- an empty array
- an object without properties

is.notEmpty: Checks if a value is not empty.
Empty means:

- an empty string
- undefined
- null
- an empty array
- an object without properties

is.date: Checks if a value represents a date (by duck typing).
If the parameter has properties of type Function for:

- getFullYear
- getMonth
- getDate

then we can use it as a Date.

is.plainObject: Checks if the value represents a plain object (something not extending the Object class).

# example

``` js
import is from 'is';
const plainObject = is.plainObject({x: 1, y: 'x', z: []}); // true
```

# install

With [npm](https://npmjs.org) do:

```
npm install is
```

# test

With [npm](https://npmjs.org) do:

```
npm test
```