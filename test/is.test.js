import { expect } from '@open-wc/testing';
import is from '../src/is.js';

describe('is.belgianCustomer', () => {
  const scenarios = [
    { description: 'ING, global window, known TLD NL', expectation: false },
    {
      description: 'ING, known TLD NL',
      window: { location: { hostname: 'test.mijn.ing.nl' } },
      expectation: false,
    },
    {
      description: 'ING, known TLD BE',
      window: { location: { hostname: 'test.mijn.ing.be' } },
      expectation: true,
    },
    {
      description: 'ING, unknown TLD',
      window: { location: { hostname: 'test.mijn.ing.de' } },
      document: { cookie: 'ing-mocks-market=NL' },
      expectation: false,
    },
    {
      description: 'ING, no hostname',
      window: { location: {} },
      document: { cookie: 'ing-mocks-market=BE' },
      expectation: true,
    },
    {
      description: 'Not ING, cookie found, NL',
      window: { location: { hostname: 'test.mijn.nl' } },
      document: { cookie: 'ing-mocks-market=NL' },
      expectation: false,
    },
    {
      description: 'Not ING, cookie found, BE',
      window: { location: { hostname: 'test.mijn.be' } },
      document: { cookie: 'ing-mocks-market=BE' },
      expectation: true,
    },
    {
      description: 'Not ING, cookie not found',
      window: { location: { hostname: 'test.mijn.be' } },
      document: {},
      expectation: false,
    },
  ];
  scenarios.forEach(scenario => {
    it(scenario.description, () => {
      expect(is.belgianCustomer(scenario.window, scenario.document)).to.equal(scenario.expectation);
    });
  });
});

describe('is.mod11Compliant', () => {
  const scenarios = [
    {
      description: 'No checkValue',
      checkValue: undefined,
      value: '003201301234567',
      weights: [2, 4, 8, 5, 10, 9, 7, 3, 6, 1],
      expectation: false,
    },
    {
      description: 'No value',
      checkValue: '5',
      value: undefined,
      weights: [2, 4, 8, 5, 10, 9, 7, 3, 6, 1],
      expectation: false,
    },
    {
      description: 'Empty value',
      checkValue: '5',
      value: '',
      weights: [2, 4, 8, 5, 10, 9, 7, 3, 6, 1],
      expectation: false,
    },
    {
      description: 'No weights',
      checkValue: '5',
      value: '003201301234567',
      weights: undefined,
      expectation: false,
    },
    {
      description: 'Weights empty',
      checkValue: '5',
      value: '003201301234567',
      weights: [],
      expectation: false,
    },
    {
      description: 'Weights not an array',
      checkValue: '5',
      value: '003201301234567',
      weights: { a: 0, b: 1 },
      expectation: false,
    },
    {
      description: 'Not compliant',
      checkValue: '2',
      value: '203847157010003',
      weights: [2, 4, 8, 5, 10, 9, 7, 3, 6, 1],
      expectation: false,
    },
    {
      description: 'Compliant',
      checkValue: '7',
      value: '234567890123456',
      weights: [2, 4, 8, 5, 10, 9, 7, 3, 6, 1],
      expectation: false,
    },
    {
      description: 'Compliant',
      checkValue: '8',
      value: '234567890123456',
      weights: [2, 4, 8, 5, 10, 9, 7, 3, 6, 1],
      expectation: true,
    },
    {
      description: 'Compliant',
      checkValue: '5',
      value: '000056789012345',
      weights: [2, 4, 8, 5, 10, 9, 7, 3, 6, 1],
      expectation: true,
    },
    {
      description: 'Compliant',
      checkValue: '5',
      value: '003201301234567',
      weights: [2, 4, 8, 5, 10, 9, 7, 3, 6, 1],
      expectation: true,
    },
  ];
  scenarios.forEach(scenario => {
    it(scenario.description, () => {
      expect(is.mod11Compliant(scenario.checkValue, scenario.value, scenario.weights)).to.equal(
        scenario.expectation,
      );
    });
  });
});

describe('is.mod97Compliant', () => {
  const scenarios = [
    { description: 'No value', value: undefined, expectation: false },
    { description: 'Empty value', value: '', expectation: false },
    { description: 'Not allowed characters', value: '1234%56', expectation: false },
    { description: 'Array', value: [0, 1], expectation: false },
    { description: 'Object', value: { a: 0, b: 1 }, expectation: false },
    { description: 'Not compliant', value: 'NL12ABNA7209131833', expectation: false },
    { description: 'Compliant', value: 'NL11ABNA7209131833', expectation: false },
  ];
  scenarios.forEach(scenario => {
    it(scenario.description, () => {
      expect(is.mod97Compliant(scenario.value)).to.equal(scenario.expectation);
    });
  });
});

describe('is.ibanAccount', () => {
  const scenarios = [
    { description: 'No parameter', parameter: undefined, expectation: false },
    { description: 'Parameter is a number', parameter: 1, expectation: false },
    { description: 'Parameter is a boolean', parameter: true, expectation: false },
    { description: 'Parameter is an array', parameter: [1], expectation: false },
    { description: 'Parameter is an object', parameter: { a: 1 }, expectation: false },
    { description: 'Parameter is an alpha string', parameter: 'a', expectation: false },
    {
      description: 'Parameter is a valid IBAN',
      parameter: 'LU120104417859198298',
      expectation: true,
    },
    {
      description: 'Parameter is a valid IBAN',
      parameter: 'NL11ABNA7209131833',
      expectation: true,
    },
    { description: 'Parameter is a valid IBAN', parameter: 'BE60812642523370', expectation: true },
    {
      description: 'Parameter is a valid IBAN',
      parameter: 'DE07500105176519718978',
      expectation: true,
    },
  ];
  scenarios.forEach(scenario => {
    it(scenario.description, () => {
      expect(is.ibanAccount(scenario.parameter)).to.equal(scenario.expectation);
    });
  });
});

describe('is.threeFiveAccount', () => {
  const scenarios = [
    { description: 'No parameter', parameter: undefined, expectation: false },
    { description: 'Parameter is a number', parameter: 1, expectation: false },
    { description: 'Parameter is a boolean', parameter: true, expectation: false },
    { description: 'Parameter is an array', parameter: [1], expectation: false },
    { description: 'Parameter is an object', parameter: { a: 1 }, expectation: false },
    { description: 'Parameter is an alpha string', parameter: 'a', expectation: false },
    {
      description: 'Parameter does not start with a letter',
      parameter: '012312345',
      expectation: false,
    },
    { description: 'Parameter is too short', parameter: 'X1231234', expectation: false },
    { description: 'Parameter is too long', parameter: 'Y123123456', expectation: false },
    { description: 'Parameter is a valid 3-5 number', parameter: 'Z12312345', expectation: true },
  ];
  scenarios.forEach(scenario => {
    it(scenario.description, () => {
      expect(is.threeFiveAccount(scenario.parameter)).to.equal(scenario.expectation);
    });
  });
});

describe('is.dutchReference', () => {
  const scenarios = [
    { description: 'No value', value: undefined, expectation: false },
    { description: 'Empty string', value: '', expectation: false },
    { description: 'Too short', value: '123456', expectation: false },
    { description: 'Too long', value: '12345678901234567', expectation: false },
    { description: 'Not allowed, length = 8', value: '12345678', expectation: false },
    { description: 'Not allowed, length = 15', value: '123456789012345', expectation: false },
    { description: 'No check digit and no length digit', value: '1234567', expectation: true },
    { description: 'Wrong length digit, length = 9', value: '063456789', expectation: false },
    { description: 'Wrong length digit, length = 10', value: '5734567890', expectation: false },
    { description: 'Wrong length digit, length = 11', value: '78345678901', expectation: false },
    { description: 'Wrong length digit, length = 12', value: '693456789012', expectation: false },
    { description: 'Wrong length digit, length = 13', value: '2034567890123', expectation: false },
    { description: 'Wrong length digit, length = 14', value: '11345678901234', expectation: false },
    { description: 'Wrong check digit, length = 9', value: '173456789', expectation: false },
    { description: 'Wrong check digit, length = 10', value: '0834567890', expectation: false },
    { description: 'Wrong check digit, length = 11', value: '59345678901', expectation: false },
    { description: 'Wrong check digit, length = 12', value: '703456789012', expectation: false },
    { description: 'Wrong check digit, length = 13', value: '6134567890123', expectation: false },
    { description: 'Wrong check digit, length = 14', value: '22345678901234', expectation: false },
    {
      description: 'Correct check digit and length digit, length = 9',
      value: '073456789',
      expectation: true,
    },
    {
      description: 'Correct check digit and length digit, length = 10',
      value: '5834567890',
      expectation: true,
    },
    {
      description: 'Correct check digit and length digit, length = 11',
      value: '79345678901',
      expectation: true,
    },
    {
      description: 'Correct check digit and length digit, length = 12',
      value: '603456789012',
      expectation: true,
    },
    {
      description: 'Correct check digit and length digit, length = 13',
      value: '2134567890123',
      expectation: true,
    },
    {
      description: 'Correct check digit and length digit, length = 14',
      value: '12345678901234',
      expectation: true,
    },
    { description: 'Wrong check digit', value: '2203847157010003', expectation: false },
    { description: 'Wrong check digit', value: '7234567890123456', expectation: false },
    { description: 'Correct check digit', value: '8234567890123456', expectation: true },
  ];
  scenarios.forEach(scenario => {
    it(scenario.description, () => {
      expect(is.dutchReference(scenario.value)).to.equal(scenario.expectation);
    });
  });
});

describe('is.vcsReference', () => {
  const scenarios = [
    { description: 'No parameter', parameter: undefined, expectation: false },
    { description: 'Parameter is a number', parameter: 1, expectation: false },
    { description: 'Parameter is a boolean', parameter: true, expectation: false },
    { description: 'Parameter is an array', parameter: [1], expectation: false },
    { description: 'Parameter is an object', parameter: { a: 1 }, expectation: false },
    { description: 'Parameter is an alpha string', parameter: 'a', expectation: false },
    {
      description: 'Parameter is a too short numerical string',
      parameter: '30390003979',
      expectation: false,
    },
    {
      description: 'Parameter is a too long numerical string',
      parameter: '3039000397970',
      expectation: false,
    },
    {
      description: 'Parameter is a non MOD97 numerical string',
      parameter: '303900039798',
      expectation: false,
    },
    { description: 'Parameter is a valid VCS', parameter: '303900039797', expectation: true },
  ];
  scenarios.forEach(scenario => {
    it(scenario.description, () => {
      expect(is.vcsReference(scenario.parameter)).to.equal(scenario.expectation);
    });
  });
});

describe('is.empty', () => {
  const scenarios = [
    { description: 'Undefined', value: undefined, expectation: true },
    { description: 'Null', value: null, expectation: true },
    { description: 'Empty string', value: '', expectation: true },
    { description: 'Filled string', value: 'check me', expectation: false },
    { description: 'Zero', value: 0, expectation: false },
    { description: 'Array with no elements', value: [], expectation: true },
    { description: 'Array with some elements', value: [0, 0], expectation: false },
    { description: 'Object with no properties', value: {}, expectation: true },
    { description: 'Object with some properties', value: { a: 0, b: 0 }, expectation: false },
  ];
  scenarios.forEach(scenario => {
    it(scenario.description, () => {
      expect(is.empty(scenario.value)).to.equal(scenario.expectation);
    });
  });
});

describe('is.notEmpty', () => {
  const scenarios = [
    { description: 'Undefined', value: undefined, expectation: false },
    { description: 'Null', value: null, expectation: false },
    { description: 'Empty string', value: '', expectation: false },
    { description: 'Filled string', value: 'check me', expectation: true },
    { description: 'Zero', value: 0, expectation: true },
    { description: 'Array with no elements', value: [], expectation: false },
    { description: 'Array with some elements', value: [0, 0], expectation: true },
    { description: 'Object with no properties', value: {}, expectation: false },
    { description: 'Object with some properties', value: { a: 0, b: 0 }, expectation: true },
  ];
  scenarios.forEach(scenario => {
    it(scenario.description, () => {
      expect(is.notEmpty(scenario.value)).to.equal(scenario.expectation);
    });
  });
});

describe('is.date', () => {
  const scenarios = [
    { description: 'No parameter', parameter: undefined, expectation: false },
    { description: 'Parameter is a number', parameter: 1, expectation: false },
    { description: 'Parameter is a string', parameter: '1', expectation: false },
    { description: 'Parameter is a boolean', parameter: true, expectation: false },
    { description: 'Parameter is an array', parameter: [1], expectation: false },
    {
      description: 'Parameter is an object with missing properties',
      parameter: { a: 1 },
      expectation: false,
    },
    {
      description: 'Parameter is an object with wrongly typed properties',
      parameter: { getFullYear: 2021, getMonth: 12, getDate: 1 },
      expectation: false,
    },
    {
      description: 'Parameter is an object with the right typed properties',
      parameter: { getFullYear: () => 2021, getMonth: () => 12, getDate: () => 1 },
      expectation: true,
    },
  ];
  scenarios.forEach(scenario => {
    it(scenario.description, () => {
      expect(is.date(scenario.parameter)).to.equal(scenario.expectation);
    });
  });
});

describe('is.plainObject', () => {
  const scenarios = [
    { description: 'No parameter', parameter: undefined, expectation: false },
    { description: 'Parameter is a number', parameter: 1, expectation: false },
    { description: 'Parameter is a string', parameter: '1', expectation: false },
    { description: 'Parameter is a boolean', parameter: true, expectation: false },
    { description: 'Parameter is an array', parameter: [1], expectation: false },
    { description: 'Parameter is a date', parameter: new Date(), expectation: false },
    { description: 'Parameter is a function', parameter: () => {}, expectation: false },
    { description: 'Parameter is a Math object', parameter: Math, expectation: true },
    { description: 'Parameter is an empty object', parameter: {}, expectation: true },
    { description: 'Parameter is an object', parameter: { a: 1 }, expectation: true },
  ];
  scenarios.forEach(scenario => {
    it(scenario.description, () => {
      expect(is.plainObject(scenario.parameter)).to.equal(scenario.expectation);
    });
  });
});
