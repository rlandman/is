/**
 * Generic "is" checks.
 *
 */
const is = {
  /**
   * Checks if a value is empty.
   * Empty means:
   * - an empty string
   * - undefined
   * - null
   * - an empty array
   * - an object without properties
   *
   * @param {*} value - Value to be checked for being empty.
   * @return {boolean} True if the value can be considered to be empty, false otherwise.
   *
   */
  empty: value => {
    return (!value && value !== 0) || (typeof value === 'object' && !Object.keys(value).length);
  },

  /**
   * Checks if a value is not empty.
   * Not empty means:
   * - a string with a length of at least 1
   * - any number
   * - an array with at least one element
   * - an object with at least one property
   *
   * @param {*} value - Value to be checked for being not empty.
   * @return {boolean} True if the value can be considered to be not empty, false otherwise.
   *
   */
  notEmpty: value => {
    return !is.empty(value);
  },

  /**
   * Use duck typing to check if the parameter behaves like a date.
   * If the parameter has properties of type Function for:
   *
   * - getFullYear
   * - getMonth
   * - getDate
   *
   * then we can use it as a Date.
   *
   * @param {Object} value - Object to be checked for having date capabilities.
   * @return {boolean} True if the value can be considered to be a Date, false otherwise.
   *
   */
  date: value => {
    const dateFunctions = ['getFullYear', 'getMonth', 'getDate'];
    return !!(
      value &&
      typeof value === 'object' &&
      dateFunctions.every(df => value[df] && typeof value[df] === 'function')
    );
  },

  /**
   * Checks if the value represents a plain object (something not extending the Object class).
   *
   * @param {Object} value - Object to be checked for being a plain object.
   * @return {boolean} True if the value can be considered to be a plain object, false otherwise.
   *
   */
  plainObject: value => {
    if (value && typeof value === 'object') {
      const ctor = value.constructor;
      const prot = ctor?.prototype;
      // eslint-disable-next-line no-prototype-builtins
      return !ctor || (typeof prot === 'object' && prot.hasOwnProperty('isPrototypeOf'));
    }

    return false;
  },
};

export default is;
